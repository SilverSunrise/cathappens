package kinect.pro.catshappens

import android.graphics.Bitmap
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kinect.pro.catshappens.repository.Repository
import kinect.pro.catshappens.repository.api.CallCatsBack
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), CallCatsBack {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val repository = Repository()
        repository.getCats(this);
    }

    override fun callCat(cat: Bitmap) {

        this@MainActivity.runOnUiThread(Runnable { imageCat.setImageBitmap(cat) })

    }
}
