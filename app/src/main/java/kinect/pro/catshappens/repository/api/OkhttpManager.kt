package kinect.pro.catshappens.repository.api

import android.graphics.BitmapFactory
import okhttp3.*
import java.io.IOException


class OkhttpManager {
    val TAG = "++"

    private var client: OkHttpClient = OkHttpClient()
    private var request: Request;
    private var url: String = "https://http.cat/101"

    init {
        request = Request.Builder()
            .url(url)
            .build()
    }

    fun getCats(callCatsBack: CallCatsBack?) {
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {

            }

            override fun onResponse(call: Call, response: Response) {
                if (response.isSuccessful) {
                    val bmp = BitmapFactory.decodeStream(response.body!!.byteStream())
                    callCatsBack?.callCat(bmp)
                }
            }
        })
    }
}







