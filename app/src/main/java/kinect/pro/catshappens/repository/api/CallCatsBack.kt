package kinect.pro.catshappens.repository.api

import android.graphics.Bitmap

interface CallCatsBack {

    fun callCat(cat: Bitmap)
}