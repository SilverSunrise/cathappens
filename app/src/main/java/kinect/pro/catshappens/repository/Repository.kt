package kinect.pro.catshappens.repository

import kinect.pro.catshappens.repository.api.CallCatsBack
import kinect.pro.catshappens.repository.api.OkhttpManager

class Repository {
    private var okhttpManager: OkhttpManager? = null

    init {
        okhttpManager = OkhttpManager();
    }

    fun getCats(callCatsBack: CallCatsBack?) {
        okhttpManager?.getCats(callCatsBack)
    }

}